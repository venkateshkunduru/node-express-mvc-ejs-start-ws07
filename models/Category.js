/** 
*  Category model
*  Describes the characteristics of each attribute in a certain category.
*
* @author Nathan Meeker <S523693@nwmissouri.edu>
*
*/

const mongoose = require('mongoose')

const CategorySchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  itemType: { type: String, required: false, default: 'description' },
  dateOrdered: { type: Date, required: true, default: Date.now() },
  priceRange: { type: Number, required: true, default: 9.99, min: 0, max: 100000 }
})

module.exports = mongoose.model('Category', CategorySchema)